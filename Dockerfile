FROM nginx:latest
COPY /index.html /usr/share/nginx/html
COPY nginx.conf /etc/nginx/
EXPOSE 8095
CMD ["nginx", "-g", "daemon off;"]
