definitions:
  services:
    ecr-repository: &ecr-repository
      name: ECR Repository
      oidc: true
      image: hashicorp/terraform
      script:
          - export AWS_REGION=$AWS_DEFAULT_REGION
          - export AWS_ROLE_ARN=$BITBUCKET_ROLE_SHARED
          - export SERVICE_NAME=$SERVICE_NAME
          - export AWS_WEB_IDENTITY_TOKEN_FILE=$(pwd)/web-identity-token
          - echo $BITBUCKET_STEP_OIDC_TOKEN > $(pwd)/web-identity-token
          - git clone git@bitbucket.org:mflorys/sh-terraform-modules.git
          - cd ./sh-terraform-modules/ecr-module
          - terraform init -backend-config=bucket=$TF_STATE_SHARED -backend-config=key=ecr/$SERVICE_NAME/$AWS_REGION/ecr.tfstate -backend-config=dynamodb_table=$TF_STATE_SHARED -backend-config=region=$AWS_REGION && terraform fmt
          - terraform validate
          - terraform plan -var bitbucket_role=$AWS_ROLE_ARN -var service_name=$SERVICE_NAME -out output.plan
          - terraform apply output.plan

    build-and-push-image: &build-and-push-image
      name: Build and Push Docker Image
      image: atlassian/default-image:3
      caches:
        - docker
      services:
        - docker
      oidc: true
      script:
        - docker build -t $SERVICE_NAME .
        - export DOCKER_TAG=$(echo $BITBUCKET_BUILD_NUMBER-$BITBUCKET_BRANCH | sed -e 's/\//-/g')
        - pipe: atlassian/aws-ecr-push-image:1.5.0
          variables:
            AWS_OIDC_ROLE_ARN: $BITBUCKET_ROLE_SHARED
            AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
            IMAGE_NAME: $SERVICE_NAME
            TAGS: $DOCKER_TAG

      task-definition: &task-definition
        name: Update Task Definition
        oidc: true
        image: atlassian/pipelines-awscli

        script:
            - export AWS_REGION=$AWS_DEFAULT_REGION
            - export AWS_ROLE_ARN=$BITBUCKET_ROLE
            - export LOCAL_SERVICE_NAME="$ENV_PREFIX"-"$SERVICE_NAME"
            - export ECR_SHARED=$ECR_SHARED
            - export DOCKER_TAG=$(echo $BITBUCKET_BUILD_NUMBER-$BITBUCKET_BRANCH | sed -e 's/\//-/g')
            - export AWS_WEB_IDENTITY_TOKEN_FILE=$(pwd)/web-identity-token
            - export IMAGE_NAME="${ECR_SHARED}/${SERVICE_NAME}:${DOCKER_TAG}"
            - echo $BITBUCKET_STEP_OIDC_TOKEN > $(pwd)/web-identity-token
            - export TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition $LOCAL_SERVICE_NAME)
            - export NEW_TASK_DEFINTIION=$(echo $TASK_DEFINITION | jq --arg IMAGE "$IMAGE_NAME" '.taskDefinition | .containerDefinitions[0].image = $IMAGE | del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities) | del(.registeredAt) | del(.registeredBy)')
            - echo $NEW_TASK_DEFINTIION > task-definition.json
            - cat task-definition.json
        artifacts:
          - task-definition.json

    deploy-to-ecs: &deploy-to-ecs
      name: Deploy to ECS
      image: atlassian/default-image:3
      oidc: true
      script:
        - pipe: atlassian/aws-ecs-deploy:1.6.2
          variables:
            AWS_OIDC_ROLE_ARN: $BITBUCKET_ROLE
            AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
            CLUSTER_NAME: $ENV_PREFIX-$ECS_CLUSTER_SUFFIX
            SERVICE_NAME: $ENV_PREFIX-$SERVICE_NAME
            FORCE_NEW_DEPLOYMENT: 'true'
            WAIT: 'true'
            TASK_DEFINITION: 'task-definition.json'


pipelines:
  branches:
    develop:
      - step:
          name: Central ECR
          deployment: SHARED-ECR
          <<: *ecr-repository
      - step:
          name: Build and push to central ECR
          deployment: SHARED
          <<: *build-and-push-image
      - step:
          name: DEV - Task Definition Deployment
          deployment: DEV-TASK-DEFINITION
          <<: *task-definition
      - step:
          name: DEV - Deploy to ECS
          deployment: DEV
          <<: *deploy-to-ecs
    feature/*:
      - step:
          name: Central ECR
          deployment: SHARED-ECR
          <<: *ecr-repository
      - step:
          name: Build and push to central ECR
          deployment: SHARED
          <<: *build-and-push-image
      - step:
          name: DEV - Task Definition Deployment
          trigger: manual
          deployment: DEV-TASK-DEFINITION
          <<: *task-definition
      - step:
          name: DEV - Deploy to ECS
          deployment: DEV
          <<: *deploy-to-ecs
      - step:
          name: STAGE - Task Definition Deployment
          deployment: STAGE-TASK-DEFINITION
          trigger: manual
          <<: *task-definition
      - step:
          name: STAGE - Deploy to ECS
          deployment: STAGE
          <<: *deploy-to-ecs
    master:
      - step:
          name: ECR
          deployment: SHARED-ECR
          <<: *ecr-repository
      - step:
          name: Build and push
          deployment: SHARED
          <<: *build-and-push-image
      - step:
          name: STAGE - Task Definition Deployment
          deployment: STAGE-TASK-DEFINITION
          <<: *task-definition
      - step:
          name: STAGE - Deploy to ECS
          deployment: STAGE
          <<: *deploy-to-ecs
      - step:
          name: PROD - Task Definition Deployment
          deployment: PROD-TASK-DEFINITION
          trigger: manual
          <<: *task-definition
      - step:
          name: PROD - Deploy to ECS
          deployment: PROD
          <<: *deploy-to-ecs